The file contains,
1. The R script 'Data_cleaning_&_organising.R - 
Steps-
A. Run the read.sheet() commands to load the datasets from Google Spreadsheets.
Libraries required - Googledrive
					 googlesheets
					 googlesheets4
					 
The Sheets that are read from the google spreadsheet must contain the following coloumns:-
		1. Sheet 1 - Quiz_all_data
		 			 a. Quiz_name
					 b. quiz cms id
					 c. studentID
					 d. student.name
					 f. score
		2. Sheet 2 - Quiz_Stats
					 a. quiz_name
					 b. No.of.takers
					 c. No.of.finishers
					 d. Average.scores
					 e. Max.Marks
B. Run rest of the entire script all together. The script will overwrite on two pre made spreadsheets,
One contains Student_detailed_results - ID  - "1mZjQ2T5qrnDp03AX6NHLBubUAeEgdJk233_i9OLsuJM"
Second contains Batch_detailed_results - ID - "1NHtRzIrvlYnD-U8yCVte9CjULNEl9xLbjXtJe5_4aSY"

2. Link to the Student_detailed_results Spreadsheet - "https://docs.google.com/spreadsheets/d/1mZjQ2T5qrnDp03AX6NHLBubUAeEgdJk233_i9OLsuJM/edit#gid=725980458"
3. Link to the Batch_detailed_results Spreadsheet   - "https://docs.google.com/spreadsheets/d/1NHtRzIrvlYnD-U8yCVte9CjULNEl9xLbjXtJe5_4aSY/edit#gid=1874876968" 

4. Link to the Google Apps scripts project-
https://script.google.com/d/1Sc4jwLCiaZ934H34-cuv32mwlMNKHPVnNpWGEMP26ONLU9a1wawD8YHl/edit?usp=sharing
The Apps script project "Generate Report" contains 5 files.
File 1 - code.gs - 
File 2 - chart.js.html
File 3 - main.js.html
File 4 - index.html
File 5 - styles.css.html

code.gs contains 4 functions, 
				   function doGet(e) - This passes the parameter as studemtID= from URL.
				   function sortDataForEachStudent(studentID) - The function sorts data for the requested studentID and 
				   			put the Data values to a temporary spreadsheet -ID- "1P5wHlrOnCfEFZenpy1dGTZaSWONTsm9k_EzqRdV4qUk", 
				   			"Sheet2".
				   function createSheet(headings,student_details) - The function pushes the data required to create graphs into the 
				   			spreadsheet -ID- "1P5wHlrOnCfEFZenpy1dGTZaSWONTsm9k_EzqRdV4qUk", "Sheet1".
				   function getChartData() - The function is called from the chart.js.html 
				   			This function extracts values from the temp sheet, "Sheet1" and returns the data to the D3 commands to create graph.
				   function getData() - The function extracts values from the temp Sheet, "Sheet2" and return the values in the index.js.html, 
				   			through which the tables is created.
chart.js.html contains 2 functions under <script></script> tags
			       var drawchart = function(data) - This function takes data from getChartData(), and creates a SVG for the graph.
main.js.html contains one major function under <script></script> tags
				   window.onload = function() {
  									google.script.run
    								.withSuccessHandler(drawChart)
    								.getChartData();
           		   };
				   The function is neccesary to display svg as the window loads.
				   Any function which requires to load as soon as the window loads needs to be defined,
				   under window.onload as google.script.run.withSuccessHandler("function to run")."function to pass data"();
index.html contains the html structure.
				   This file is returned from doGet(e) function as,
				   return HtmlService.createTemplateFromFile("index.html")
    			   .evaluate()
    			   .setSandboxMode(HtmlService.SandboxMode.IFRAME);
Styles.css.html conatins style tags for the charts and html output.

Steps to run the project:-
1. click on Publish.
2. select deploy as webApp.
3. select a new project version.
4. Select 'ME("your email ID")' under 'Execute the app as:'.
5. Select 'anyone, even anonymous' under 'Who has access to the app'.
6. Click on update.
7. Copy the URL that pops up.
		Current URL to the webApp is 
		"https://script.google.com/macros/s/AKfycby0Fxlt6XQ1CVyjcEHusOZ9P7d7fcVsF1OAq0LGGBy79KJKGIk/exec"
8. Add parameter of studentID as ?studentID="StudentID"
For example-
https://script.google.com/macros/s/AKfycby0Fxlt6XQ1CVyjcEHusOZ9P7d7fcVsF1OAq0LGGBy79KJKGIk/exec?studentID=BGM2100009

The above URL can directly be sent to the Student with ID 'BGM2100009'.
When the URL is executed the performance report which conatins:-
							1. Bar graph with 
								a.Batch Avg Marks
								b. Student's_Marks
								c. Batch_topper_Marks
								d. Total_Marks
							2. Table with
								a.Student_ID
								b.Student_marks
								c.Total_marks
								d.Topper_marks
								e.Batch_avg
								f.Rank
								g.Percentile
The above as output gets displayed on the screen.
				   